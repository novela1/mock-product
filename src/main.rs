use deltalake::arrow::{
    array::{ArrayRef, Float64Array},
    datatypes::{DataType, Field, Schema as ArrowSchema},
    record_batch::RecordBatch,
};
use std::sync::Arc;

use deltalake::{self, DeltaOps, DeltaTable, DeltaTableBuilder, DeltaTableError, SchemaDataType, SchemaField};
use rand::{distributions::Uniform, Rng};

mod app_config;

use app_config::AppConfig;

fn get_table_columns(conf: &AppConfig) -> Vec<SchemaField> {
    (0..conf.column_number)
        .map(|col_num| {
            SchemaField::new(
                String::from(format!("col_{}", col_num)),
                SchemaDataType::primitive(String::from("double")),
                false,
                Default::default(),
            )
        })
        .collect()
}

async fn open_table(conf: &AppConfig) -> Result<DeltaTable, DeltaTableError> {
    let mut table = DeltaTableBuilder::from_uri(conf.table_url.clone())
        .with_version(conf.version)
        .build()?;

    let result = match table.load().await {
        Ok(_) => Ok(table.into()),
        Err(DeltaTableError::NotATable(_)) => {
            let new_table = DeltaOps::from(table).create().with_columns(get_table_columns(conf)).await?;
            Ok(new_table)
        }
        Err(err) => Err(err),
    };
    result
}

fn generate_batch(conf: &AppConfig) -> RecordBatch {
    let schema = Arc::new(ArrowSchema::new(
        (0..conf.column_number)
            .map(|col_num| Field::new(format!("col_{}", col_num), DataType::Float64, false))
            .collect::<Vec<_>>(),
    ));

    let range = Uniform::from(-5.0f64..5f64);
    let mut rng = rand::thread_rng();
    let columns: Vec<ArrayRef> = (0..conf.column_number)
        .map(|_| -> ArrayRef {Arc::new(Float64Array::from_iter_values((&mut rng).sample_iter(&range).take(conf.batch_size)))})
        .collect();



    RecordBatch::try_new(schema, columns).unwrap()
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), deltalake::DeltaTableError> {
    let conf = AppConfig::load();
    let table = open_table(&conf).await.unwrap();
    let table = DeltaOps(table).write(vec![generate_batch(&conf)]).await?;
    let (table, _) = DeltaOps(table).vacuum().await?;
    DeltaOps(table).optimize().await?;
    Ok(())
}