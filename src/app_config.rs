use figment::{Figment, providers::Env};
use serde::{Deserialize};

#[derive(Deserialize, Debug)]
pub struct AppConfig {
    pub table_url: String,

    #[serde(default = "AppConfig::d_version")]
    pub version: i64,

    #[serde(default = "AppConfig::d_batch_size")]
    pub batch_size: usize,

    #[serde(default = "AppConfig::d_column_num")]
    pub column_number: usize,
}

impl AppConfig {

    pub fn load() -> AppConfig {
        Figment::from(Env::prefixed("NL_MOCK_PRODUCT_")).extract().unwrap()
    }

    fn d_version() -> i64 {1}
    fn d_batch_size() -> usize {1e3 as usize}
    fn d_column_num() -> usize { 8 }
}