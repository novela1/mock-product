FROM rust:1.69.0-buster AS base

RUN apt update && apt install -y upx

RUN cargo new --name nl-mock-product /app

COPY Cargo.toml Cargo.lock /app/

WORKDIR /app

RUN --mount=type=cache,target=/usr/local/cargo/registry --mount=type=cache,target=/app/target cargo build

COPY . /app

RUN --mount=type=cache,target=/usr/local/cargo/registry --mount=type=cache,target=/app/target cargo build

FROM base AS realise_build

RUN --mount=type=cache,target=/usr/local/cargo/registry --mount=type=cache,target=/app/target \
    cargo build --release && \
    cp /app/target/release/nl-mock-product /nl-mock-product

RUN upx --best --lzma /nl-mock-product

FROM debian:bullseye-slim AS realise_app

COPY --from=realise_build /nl-mock-product  /nl-mock-product
CMD ["/nl-mock-product"]

# /home/yura/.cargo/registry/src