# Load .env
ifneq ("$(wildcard .env)","")
    include .env
    export $(shell sed 's/=.*//' .env)
else
endif
.ONESHELL:
.EXPORT_ALL_VARIABLES:

WORKDIR := $(shell pwd)


help: ## Display help message
	@echo "Please use \`make <target>' where <target> is one of"
	@perl -nle'print $& if m{^[\.a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-25s\033[0m %s\n", $$1, $$2}'
	
run: ## Run the app
	cargo run
	
run_r: ## Run the realise app
	cargo run --release

build_image: ## Build the docker image
	docker build -t nl-mockproduct --target realise_app .
	docker tag nl-mockproduct europe-west3-docker.pkg.dev/novel-sandbox/novela/mockproduct
	echo "To push run \n\tdocker push europe-west3-docker.pkg.dev/novel-sandbox/novela/mockproduct"

